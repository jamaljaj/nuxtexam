import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)


export default () => {

    return new Vuex.Store({
        state: {
            listUsers: [], 
            listTransactions:[]
        },
        mutations: {
            setListUser(state, _f) {
                state.listUsers = _f;
            },
            setListTransactions(state, _f) {
                state.listTransactions = _f;
            }
        },
        actions: {
            FillListUser( context) {

              this.$axios.$get('/Person/', {

                    Price: parseFloat(this.Price), //intpa this.Price,
                    Tdate: new Date(this.Tdate),

                    Pid: this.Pid

                }).then(function (response) {
                   
                      context.commit('setListUser', response)
                })
                    .catch(function (error) {
                        console.log(error);
                    });
 

            },

            FillListTransactions( context) {

                this.$axios.$get('/Transactions/', {
  
                      Price: parseFloat(this.Price), //intpa this.Price,
                      Tdate: new Date(this.Tdate),
  
                      Pid: this.Pid
  
                  }).then(function (response) {
                     
                    console.log(response)
                        context.commit('setListTransactions', response);
                  })
                      .catch(function (error) {
                          console.log(error);
                      });
   
  
              },
              SaveNewTransation(context , data){

                this.$axios.$post('/Transactions/', {

                    Price: data.Price, //intpa this.Price,
                Tdate: new Date( data.Tdate),

                Pid: data.Pid

                }).then(function (response) {
                  
           
                })
                .catch(function (error) {
                   
                });
              },
              SaveNewUser(context , data) {
                 
                try {
    
                    this.$axios.$post('person', {
                        Name:data.Name,
                        Family:data.Family
                        });
    
                  
                } catch (e) {
                    //  this.$toast.global.my_error() //Using custom toast
                    this.$toast.error('Error')
                } 
                  
            },
        },
        getters: {
            getListUser(state) {
                return state.listUsers;
            },
            getListTransactions(state) {
                return state.listTransactions;
            }
        }

    })


}